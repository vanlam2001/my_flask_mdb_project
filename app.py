from flask import Flask, request, redirect, url_for, render_template
from pymongo import MongoClient
from bson.objectid import ObjectId

app = Flask(__name__)

# kết nối đến MongoDB 
client = MongoClient('mongodb+srv://vanlam1412:0903992237@cluster0.8eouv1s.mongodb.net')
db = client['task_manager']
collection = db['tasks']

@app.route('/', methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        task = request.form['task']
        collection.insert_one({'task': task})
        return redirect(url_for('home'))
    else:
        tasks = list(collection.find())
        return render_template('index.html', tasks=tasks)

@app.route('/delete/<task_id>', methods=['GET'])
def delete_task(task_id):
    collection.delete_one({'_id': ObjectId(task_id)})
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(debug=True)

    

